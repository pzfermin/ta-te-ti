
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;



/**
 *
 * @author ferminpaez
 */
public class TicTacToe extends javax.swing.JFrame implements ActionListener {

    Casilla [][] TABLERO;
    int contador;
    
    public TicTacToe() {
        initComponents();
        TABLERO = new Casilla [3][3];
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                TABLERO[i][j] = new Casilla();
                TABLERO[i][j].A.setBounds((i*100), (j*100), 100, 100);
                TABLERO[i][j].A.addActionListener(this);
                this.add(TABLERO[i][j].A);
            }
            
        }
     
    }
    


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(320, 340));
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TicTacToe().setVisible(true);
            }
        });
    }
    void Tiro(Casilla X){
        ImageIcon ICONO = null;
        if (contador%2==0)
        {
            ICONO = new ImageIcon(this.getClass().getResource("O.png"));
            X.B=1;
        }
        else
        {
            ICONO = new ImageIcon(this.getClass().getResource("X.png"));
            X.B=4;
        }
        ICONO = new ImageIcon(ICONO.getImage().getScaledInstance(90, 90, java.awt.Image.SCALE_DEFAULT));
        X.A.setIcon(ICONO);
        X.A.removeActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        for (int i = 0; i < 3; i++) 
        {
            for (int j = 0; j < 3; j++)
            {
                
                if (e.getSource()== TABLERO[i][j].A)
                {
                    Tiro(TABLERO[i][j]);
                    if(Revisar())
                    {
                        JOptionPane.showMessageDialog(null,"Ganaste");
                     
                    }
                    contador++;
                }
                
            }
        }
    }
    
    boolean Revisar(){
        boolean Gano = false;
        int Suma = 0;
        for (int i = 0; i < 3; i++) 
        {
            Suma=TABLERO[i][0].B+TABLERO[i][1].B+TABLERO[i][2].B;
            if(Suma==3 || Suma==12)
            {
                Gano = true;
                break;
            }
            
        }
        for (int i = 0; i < 3; i++)
        {
            Suma=TABLERO[0][i].B+TABLERO[1][i].B+TABLERO[2][i].B;
            if(Suma==3 || Suma == 12)
            {
                Gano = true;
                break;
            }
            
        }
        Suma = TABLERO[0][2].B+TABLERO[1][1].B+TABLERO[2][0].B;
        if (Suma == 3 || Suma == 12 )
        {
            Gano = true;
        }
        for (int i = 0; i < 3; i++) 
        {
            Suma+=TABLERO[i][i].B;  
        }
        if (Suma == 3 || Suma == 12)
        {
            Gano = true;
        }
                return Gano;
    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
